#ifndef SHARED_MEMORY_
#define SHARED_MEMORY_

#ifdef __cplusplus
extern "C" {
#endif

void *do_work(void *data);

#ifdef __cplusplus
}
#endif
#endif /* SHARED_MEMORY_ */

