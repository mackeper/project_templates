#include "addition.hpp"
#include "gtest/gtest.h"


TEST(AdditionTest, Add)
{
    EXPECT_EQ(0, Math::add_three(0,  0, 0));
    EXPECT_EQ(0, Math::add_three(0, -1, 1));
    EXPECT_EQ(8, Math::add_three(0,  3, 5));
}

