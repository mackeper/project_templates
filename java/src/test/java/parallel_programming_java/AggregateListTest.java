package parallel_programming_java;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.*;

public class AggregateListTest {
    @Test public void testEmptyAggregate() {
        ArrayList<Integer> arr = new ArrayList<Integer>();
        assertEquals(0, AggregateList.aggregate(arr));
    }

    @Test public void testSingleAggregate() {
        ArrayList<Integer> arr = new ArrayList<Integer>();
        arr.add(1);
        assertEquals(1, AggregateList.aggregate(arr));
    }

    @Test public void testMultipleAggregate() {
        ArrayList<Integer> arr = new ArrayList<Integer>();
        int max_value = 10000000;
        int sum = 0;
        for (int i = 1; i < max_value; i++)
            arr.add(i);
        for (int i = 1; i < max_value; i++)
            sum += i;
        assertEquals(sum, AggregateList.aggregate(arr));
    }
}
