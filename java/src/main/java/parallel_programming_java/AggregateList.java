package parallel_programming_java;
import java.util.*;

class AggregationThread extends Thread {
    private ArrayList<Integer> arr_;
    private int sum_ = 0, start_, size_;

    public AggregationThread(ArrayList<Integer> arr, int start, int size, ThreadGroup tg) {
        super(tg, "Aggregation_start="+start);
        arr_ = arr;
        start_ = start;
        size_ = size;
    }

    public void run() {
        for (int i = start_; i < start_ + size_; i++) {
            sum_ += arr_.get(i);
        }
    }

    public int getResult() {
        return sum_;
    }
}

public class AggregateList {


    public static int aggregate(ArrayList<Integer> arr) {
        long startTime = System.nanoTime();

     	ThreadGroup tg = new ThreadGroup("main");

        int sum = 0;
        int num_threads = Math.min(arr.size(), 8);
        num_threads = 1;
        int size_per_thread = (int)Math.ceil((double)arr.size()/(double)num_threads);
        ArrayList<AggregationThread> threads = new ArrayList<AggregationThread>();
        for (int i = 0; i < num_threads; i++) {
            int tmp_size = Math.min(size_per_thread, arr.size() - i*size_per_thread);
            threads.add(new AggregationThread(arr, i*size_per_thread, tmp_size, tg));
            threads.get(threads.size()-1).start();
        }

        while (tg.activeCount() > 0) {} // wait

        for (int i = 0; i < num_threads; i++) {
            sum += threads.get(i).getResult();
        }

        long endTime = System.nanoTime();
        long duration = (endTime - startTime);  //divide by 1000000 to get milliseconds.

        return sum;
    }

}
